var express = require('express');
var router = express.Router();
const defaultController = require('../controller/DefaultController');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Time Lapse Smart Staff' });
});

router.post('/takeapicture', function(req, res, next) {
  res.send(defaultController.recursiva(req));
  //res.render('index', { title: 'Express' });
});

router.post('/listar', function(req, res, next) {
  res.send(defaultController.listar(req.body.camera));
  //res.render('index', { title: 'Express' });
});

module.exports = router;
